<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** LOGIN **/

Route::post('login', function(){
	return response()->file(storage_path("json/login.json"));
});


Route::get('transactionconciliations', 'TransactionConciliationsController@response');
Route::get('transactionsummaries', 'TransactionSummariesController@response');
Route::get('movementsummaries', 'MovementSummariesController@response');
Route::get('financials', 'FinancialsController@response');
Route::get('adjustsummaries', 'AdjustSummariesController@response');
Route::get('adjusts', 'AdjustsController@response');
	 






